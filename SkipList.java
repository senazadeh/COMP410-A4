package SKPLIST_A4;

import java.util.Arrays;
import java.util.Random;

public class SkipList implements SkipList_Interface {
  private SkipList_Node root;
  private final Random rand;
  private double probability;
  private int MAXHEIGHT = 30; // the most links that a data cell may contain
  private int size = 0;

  public SkipList(int maxHeight) {
    root = new SkipList_Node(Double.NaN, maxHeight);
    rand = new Random();
    probability = 0.5;
  }

  @Override
  public void setSeed(long seed) { rand.setSeed(seed); }
  
  @Override
  public void setProbability(double probability) { 
     this.probability = probability; 
  }
  
  private boolean flip() {
    // use this where you "roll the dice"
    // call it repeatedly until you determine the level
    // for a new node
    return rand.nextDouble() < probability;
  }
  
//  getRoot:
//	    in: none
//	    return: a skiplist node, the one that is the starter of the entire skiplist
//	            the skiplist starts with a sentinel, made in the list constructor.
//	            so getRoot returns the sentinel always, even if the skiplist is empty.
//	            If the list is empty, then level 0 of the senetinel link array would be null.
//	    effect: no change to skiplist state
	    
  public SkipList_Node getRoot() { return root; }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    
    int levels;
    for(levels = 0; levels < root.getNext().length && root.getNext(levels) != null; levels ++);
    
    StringBuilder[] sbs = new StringBuilder[levels];
    
    for(int i = 0; i < sbs.length; i ++) {
      sbs[i] = new StringBuilder();
      sbs[i].append("level ").append(i).append(":");
    }
    
    SkipList_Node cur = root;
    
    while (cur.getNext(0) != null) {
      cur = cur.getNext(0);
      for(int i = levels - 1; i >= cur.getNext().length; i --) {
        sbs[i].append("\t");
      }
      for(int i = cur.getNext().length - 1; i >= 0; i --) {
        if (cur.getNext(i) == null) {
          levels --;
        }
        sbs[i].append("\t").append(cur.getValue());
      }
    }
    
    for(int i = sbs.length - 1; i >= 0; i --) {
      sb.append(sbs[i]).append("\n");
    }
    
    return sb.toString();
  }

  
//  insert:
//	    in: a douoble (the element to be stored into the skiplist)
//	    return: boolean, return true if insert is successful, false otherwise
//	    effect: if the double is already in the skiplist, then there is no change to
//	            the skiplist state, and return false
//	            if the double is not already in the skiplist, then a new skiplist node
//	              is created, the double put into it as data, the new node is linked
//	              into the skiplist at the proper place; size is incremented by 1,
//	              and return a true
	
	public boolean insert(double value) {
		if (contains(value)) return false; 
		
		int nodeHeight = 1;
		while (flip()) nodeHeight++; 
		if (nodeHeight > MAXHEIGHT) nodeHeight = MAXHEIGHT;
		
		SkipList_Node newNode = new SkipList_Node(value, nodeHeight);
		SkipList_Node current = root;
		
		int level = nodeHeight-1;
		
		while (level >= 0) {
			if (current.getNext(level) == null) {
				current.setNext(level, newNode);
				level--;
				continue;
			}
			if (current.getNext(level).getValue() > value) {
				SkipList_Node temp = current.getNext(level);
				current.setNext(level, newNode);
				current.getNext(level).setNext(level, temp);
				level--;
				continue; 
			}
			if (current.getNext(level).getValue() < value) {
				current = current.getNext(level);
				continue;
			}
		}
		size++;
		return true;
	}
	
//	  remove:
//		    in: a double (the element to be taken out of the skiplist)
//		    return: boolean, return true if the remove is successful, false otherwise
//		            this means return false if the skiplist size is 0
//		    effect: if the element being looked for is in the skiplist, unlink the node containing
//		            it and return true (success); size decreases by one
//		            if the element being looked for is not in the skiplist, return false and
//		            make no change to the skiplist state	
	
	public boolean remove(double value) {
		if (size == 0) return false;
		if (!contains(value)) return false; 
		
		SkipList_Node current = root;
		int level = level();
		
		while (level >= 0) {
			if (current.getNext(level) == null) {
				level--;
				continue;
			}
			if (current.getNext(level).getValue() == value) {
				current.setNext(level, current.getNext(level).getNext(level));
				level--;
				continue;
			}
			if (current.getNext(level).getValue() > value) {
				level--;
				continue;
			}
			if (current.getNext(level).getValue() < value) {
				current = current.getNext(level);
				continue;
			}
		}
		size--;
		return true;
	}
	
//	 contains:
//		    in: a double (the element to be seaarched for)
//		    return: boolean, return true if the double being looked for is in the skiplist;
//		            return false otherwise
//		            this means return false if skiplist size is 0
//		    effect: no change to the state of the skiplist
	
	public boolean contains(double value) {
		if (size == 0) return false; 
		
		SkipList_Node current = root;
		int level = level();
		
		while (level >= 0) {
			if (current.getNext(level) == null) {
				level--;
				continue;
			}
			if (current.getNext(level).getValue() == value) {
				return true;
			}
			if (current.getNext(level).getValue() > value) {
				level--;
				continue;
			}
			if (current.getNext(level).getValue() < value) {
				current = current.getNext(level);
				continue;
			}
		}	
		return false;
	}
	 
//	  findMin:
//		    in: none
//		    return: double, the element value from the skiplist that is smallest
//		    effect: no skiplist state change
//		    error: is skiplist size is 0, return Double.NaN
		    		
	public double findMin() {
		if (size == 0) return Double.NaN; 
		return root.getNext(0).getValue();
	}
	
//	 findMax:
//		    in: none
//		    return: double, the element value from the skiplist that is largest
//		    effect: no skiplist state change
//		    error: is skiplist size is 0, return Double.NaN
		    		
	public double findMax() {
		if (size == 0) return Double.NaN; 
		
		SkipList_Node current = root;
		int level = level();
		
		while (level >= 0) {
			if (current.getNext(level) != null) {
				current = current.getNext(level); 
			} else {  
				level--; 
			}
		}	
		return current.getValue();
	}
	 
//	  empty:
//		    in: nothing
//		    return: boolean, true if the skiplist has no elements in it, true if size is 0
//		            return false otherwise
//		    effect: no change to skiplist state
		    
	public boolean empty() {
		if (size == 0) return true; 
		return false;
	}
	
//	 clear:
//		    in: none
//		    return: void
//		    effect: all elements in the skip list are unhooked so that no elements are in the list
//		            size is set to 0
//		            sentinel node remains
//		            the effect is to create a skip list state that exists when it is first 
//		            produced by the constructor
		            
	public void clear() {
		root = new SkipList_Node(Double.NaN, MAXHEIGHT);
		size = 0;
	}
	
//	  size:
//		    in: nothing
//		    return: number of elements stored in the skiplist
//		    effect: no change to skiplist state

	public int size() {
		return size;
	}
	
//	 level:
//		    in: none
//		    return: integer, the level of the highest node in the skiplist
//		    effect: no change in skiplist state
//		    error: return -1 if skiplist is empty (size is 0, only sentinel node is there)
		    		
	public int level() {
		if (size == 0) return -1; 
		int i = 0;
		while (root.getNext(i) != null)  {
			i++; 
			if (i >= root.getNext().length) return root.getNext().length-1;
		}

		return i-1;
	}
	 
//	 max:  
//		    in: none
//		    return: integer, the value of MAXHEIGHT that is set in the list constructor
//		    effect: no change in skip list state
		    
	public int max() {
		return MAXHEIGHT;
	}
	
  //---------------------------------------------------------
  // student code follows
  // implement the methods of the interface
  //---------------------------------------------------------

}
